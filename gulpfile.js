const { src, dest, watch, series, parallel } = require('gulp');

const sass = require('gulp-dart-sass');

const autoprefixer = require('gulp-autoprefixer');

const csso = require('gulp-csso');

const rename = require('gulp-rename');

const babel = require('gulp-babel');

const terser = require('gulp-terser');

const webpack = require('webpack-stream');

const sourcemaps = require('gulp-sourcemaps');

const del = require('del');

const mode = require('gulp-mode')();

const browserSync = require('browser-sync').create();


// clean task

const clean = () => {
    return del(['dest']);
}

const cleanImages = () => {
    return del(['dest/assets/images']);
}

const cleanFonts = () => {
    return del(['dest/assets/fonts']);
}

// css tasks

const css = () => {
    return src('src/scss/index.scss')
    .pipe(mode.development( sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(rename('app.css'))
    .pipe(mode.production( csso()))
    .pipe(mode.development( sourcemaps.write()))
    .pipe(dest('dest'))
    .pipe(mode.development(browserSync.stream()));
}

// js tasks

const js = () => {
    return src('src/**/*.js')
    .pipe(babel({
        presets: ['@babel/env']
    }))
    .pipe(webpack({
        mode: 'development',
        devtool: 'inline-source-map'
    }))
    .pipe(mode.development( sourcemaps.init({ loadMaps: true})))
    .pipe(rename('app.js'))
    .pipe(mode.production( terser({ output: { comments: false}}) ))
    .pipe(mode.development( sourcemaps.write()))
    .pipe(dest('dist'))
    .pipe(mode.development( browserSync.stream()));
}
// copy tasks

const copyImages = () => {
    return src('src/assets/images/**/*.{jpg,jpeg,png,gif,svg}')
    .pipe(dest('dest/assets/images'));
}

const copyFonts = () => {
    return src('src/assets/fonts/**/*.{svg,eot,ttf,woff,woff2}')
    .pipe(dest('dest/assets/fonts'));
}

// watch tasks

const wathcForChanges = () => {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });

    watch('src/scss/**/*.scss', css);
    watch('src/**/*.js', js);
    watch('**/*.html').on('change', browserSync.reload);
    watch('src/assets/images/**/*.{png,jpg,jpeg,gif,svg}', series(cleanImages, copyImages));
    watch('src/assets/fonts/**/*.{svg,eot,ttf,woff,woff2}', series(cleanFonts, copyFonts));
}

// public tasks

exports.default = series(clean, parallel(css, js, copyImages, copyFonts), wathcForChanges);
exports.build = series(clean, parallel(css, js, copyImages, copyFonts));
